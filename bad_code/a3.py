import subprocess
from openpyxl import Workbook
import csv
import re
import numpy as np
import pandas as pd
import sys
import tiktoken
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import CountVectorizer

#separator_tshark = "\x1F"
separator_tshark = "!"
separator_python = "!"
output_file = "ftp_commands.txt"
output_file_test = "ftp_commands_test.txt"


tokenizer = tiktoken.get_encoding("cl100k_base")


def read_file_into_2d_array(file_path, offset=0):
    two_d_array = []
    with open(file_path, "r") as file:
        for line in file:
            fields = line.split(separator_python, 7)
            # Splitting line using both tabs and spaces
            fields = [field if field != " " else "" for field in fields]
            if len(fields) != 8: raise Exception("Too many values: " + str(len(fields)) + " " + str(fields)) 
            two_d_array.append(fields)
    
    return two_d_array


def clean_array(two_d_array, offset=0):
    cleaned_array = []
    for i, row in enumerate(two_d_array):
        if any(row[5]):
            if i > 0:
                # Move the values to the row above
                cleaned_array[-1][5] = row[5]
                cleaned_array[-1][6] = row[6]
            # Skip adding this row to the cleaned array
            continue
        # Otherwise, add the row to the cleaned array
        #row[6] = str(offset + int(row[6]) )
        cleaned_array.append(row)
                    
    cleaned_array = np.array(cleaned_array)
    cleaned_array[:, 6] = (cleaned_array[:, 6].astype(int) + offset).astype(str)
    return cleaned_array   


two_d_array_train = read_file_into_2d_array(output_file)
clean_two_d_array_train = clean_array(two_d_array_train)
two_d_array_test = read_file_into_2d_array(output_file_test)
clean_two_d_array_test = clean_array(two_d_array_test,10000)


data_test = pd.DataFrame(clean_two_d_array_test, columns=['src_ip', 'dest_ip', 'src_port', 'dest_port', 'command', 'response_code', 'stream', 'arg'])
data_train = pd.DataFrame(clean_two_d_array_train, columns=['src_ip', 'dest_ip', 'src_port', 'dest_port', 'command', 'response_code', 'stream', 'arg'])

data_test = data_test.iloc[:, 4:]
data_train = data_train.iloc[:, 4:]

data_train['arg'] = data_train['arg'].astype(str).apply(lambda x: x.encode('utf-8'))
data_test['arg'] = data_test['arg'].astype(str).apply(lambda x: x.encode('utf-8'))


print(data_test['arg'].head())

# Tokenize the arguments using tiktokenizer




