import csv

# Function to read CSV file into a dictionary
def read_csv_file(filename):
    data = {}
    with open(filename, 'r') as file:
        reader = csv.reader(file)
        for row in reader:
            data[row[0]] = row[1]
    return data

# Function to write missing entries to a new CSV file
def write_missing_entries(filename, missing_entries):
    with open(filename, 'w', newline='') as file:
        writer = csv.writer(file)
        for key, value in missing_entries.items():
            writer.writerow([key, value])

# Read the contents of std_output_final.csv and dings.csv into dictionaries
std_output_final = read_csv_file('std_output_final.csv')
dings = read_csv_file('result_task_1_complete_20240524_214254.csv')

# Find missing entries in std_output_final.csv
missing_in_std_output_final = {key: value for key, value in dings.items() if key not in std_output_final}

# Find missing entries in dings.csv
missing_in_dings = {key: value for key, value in std_output_final.items() if key not in dings}

# Write missing entries to new CSV files
write_missing_entries('missing_in_std_output_final.csv', missing_in_std_output_final)
write_missing_entries('missing_in_dings.csv', missing_in_dings)        