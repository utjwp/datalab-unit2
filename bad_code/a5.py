import pyshark
import pandas as pd
from sklearn.ensemble import IsolationForest
from sklearn.preprocessing import StandardScaler
import numpy as np
from multiprocessing import Pool
import pickle

def process_packet(packet):
    if hasattr(packet, 'tcp_raw'):
        stream_id = packet.tcp.stream
        try:
            raw_data = bytes.fromhex(packet.tcp_raw.replace(':', ''))
            return stream_id, raw_data
        except AttributeError:
            pass
    return None, None

def extract_stream_data(pcap_file,):
    cap = pyshark.FileCapture(pcap_file, use_json=True,include_raw=True)
    packets = list(cap)

    with Pool() as pool:
        results = pool.map(process_packet, packets)

    streams = {}
    for stream_id, raw_data in results:
        if stream_id and raw_data:
            if stream_id in streams:
                streams[stream_id] += raw_data
            else:
                streams[stream_id] = raw_data

    return streams

# Load and concatenate data from the pcap files
try:
    # Try to load dictionary data from file
    with open('stream_data.pkl', 'rb') as f:
        all_streams = pickle.load(f)
except FileNotFoundError:
    print("loading data from pcap files")
    # Process pcap files and save dictionary data to file
    test_streams = extract_stream_data('test.pcap')
    print("test streams processed")
    with open('stream_data_test.pkl', 'wb') as f:
        pickle.dump(test_streams, f)
    print("test streams saved")
    train_streams = extract_stream_data('train.pcap')
    print("train streams processed")
    with open('stream_data_train.pkl', 'wb') as f:
        pickle.dump(train_streams, f)     
    print("train streams saved")
    all_streams = {**train_streams, **test_streams}
    with open('stream_data_all.pkl', 'wb') as f:
        pickle.dump(all_streams, f)

      

# Concatenate the binary data for each stream into a single dataset
all_streams = {**train_streams, **test_streams}

# Create a DataFrame with each stream's binary data
data = pd.DataFrame(list(all_streams.items()), columns=['stream_id', 'data'])

# Convert binary data to a list of integers for each stream
data['data'] = data['data'].apply(lambda x: list(x))

# Pad or truncate the binary sequences to a fixed length (e.g., 500 bytes)
max_length = 500
data['data'] = data['data'].apply(lambda x: x[:max_length] if len(x) > max_length else x + [0] * (max_length - len(x)))

# Convert to a 2D numpy array
binary_data = np.array(data['data'].tolist())

# Standardize the data
scaler = StandardScaler()
binary_data = scaler.fit_transform(binary_data)

# Train an anomaly detection model
model = IsolationForest(contamination=0.1, random_state=42)
model.fit(binary_data)

# Detect anomalies
anomalies = model.predict(binary_data)

# Add the anomaly labels to the DataFrame
data['anomaly'] = anomalies

# Print the results
print(data[['stream_id', 'anomaly']])