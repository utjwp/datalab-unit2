import subprocess
from openpyxl import Workbook
import csv
import re
import numpy as np
import pandas as pd
import sys
import tiktoken
import sentencepiece as spm

#separator_tshark = "\x1F"
separator_tshark = "!"
separator_python = "!"
output_file = "ftp_commands.txt"
output_file_test = "ftp_commands_test.txt"
output_excel = "output_file2.xlsx" 
#output_excel = "output_file_jimi_blue.xlsx" 
output_excel_test = "output_file2_test.xlsx"
def remove_non_printable_chars(s):
    return s#re.sub(r'[^\x20-\x7E]+', '', s)

tokenizer = tiktoken.get_encoding("cl100k_base")

def convert_to_byte_string(value):
    """Convert a value to a byte string if it is a string."""
    if isinstance(value, str):
        return value.encode('utf-8')
    return value

def convert_from_byte_string(value):
    """Convert a byte string back to a regular string if it is a byte string."""
    if isinstance(value, bytes):
        return value.decode('utf-8', errors='replace')
    return value

def write_byte_string_to_excel(df, file_name):
    # Convert the DataFrame to byte strings
    byte_df = df.applymap(convert_to_byte_string)

    # Create a new workbook and select the active worksheet
    wb = Workbook()
    ws = wb.active

    # Write the headers
    ws.append(byte_df.columns.tolist())

    # Write the data
    for index, row in byte_df.iterrows():
        decoded_row = [convert_from_byte_string(cell) for cell in row]
        ws.append(decoded_row)

    # Save the workbook
    wb.save(file_name)
    print(f"Data successfully written to {file_name}")



def read_file_into_2d_array(file_path, offset=0):
    two_d_array = []
    with open(file_path, "r") as file:
        for line in file:
            fields = line.split(separator_python, 7)
            # Splitting line using both tabs and spaces
            fields = [field if field != " " else "" for field in fields]
            if len(fields) != 8: raise Exception("Too many values: " + str(len(fields)) + " " + str(fields)) 
            two_d_array.append(fields)
    
    return two_d_array


def clean_array(two_d_array, offset=0):
    cleaned_array = []
    for i, row in enumerate(two_d_array):
        if any(row[5]):
            if i > 0:
                # Move the values to the row above
                cleaned_array[-1][5] = row[5]
                cleaned_array[-1][6] = row[6]
            # Skip adding this row to the cleaned array
            continue
        # Otherwise, add the row to the cleaned array
        #row[6] = str(offset + int(row[6]) )
        cleaned_array.append(row)
                    
    cleaned_array = np.array(cleaned_array)
    cleaned_array[:, 6] = (cleaned_array[:, 6].astype(int) + offset).astype(str)
    return cleaned_array   

def write_2d_array_to_excel(two_d_array, output_excel):
    # Create a new workbook
    wb = Workbook()
    # Select the active worksheet
    ws = wb.active

    # Process the 2D array

    for row in two_d_array:
        ws.append(row)

    # Save the workbook
    wb.save(output_excel)

def write_np_array_to_excel(array, filename):
    # Convert the NumPy array to a DataFrame
    df = pd.DataFrame(array)

    # Write the DataFrame to an Excel file without headers
    df.to_excel(filename, header=False, index=False)   

# Example usage

def concatenate_path(wd, arg):
    if wd == "/":
        return wd + arg
    else:
        return wd + "/" + arg



def concatenate_arrays(array1, array2):
    # Ensure the arrays are compatible for concatenation along the first axis (rows)
    if array1.shape[1] != array2.shape[1]:
        raise ValueError("Arrays must have the same number of columns to concatenate.")
    
    # Concatenate the arrays
    concatenated_array = np.concatenate((array1, array2), axis=0)
    
    return concatenated_array

def filter_ftp_commands(output_file, output_file_test):

    #uftp.keyinfo.tstamp empty
    tshark_command = f"/opt/homebrew/bin/tshark -r train.pcap -Y 'ftp' -T fields -e ip.src -e ip.dst -e tcp.srcport -e tcp.dstport -e ftp.request.command -e ftp.response.code -e tcp.stream -e ftp.request.arg -E header=n -E separator={separator_tshark}  "
    tshark_command_test = f"/opt/homebrew/bin/tshark -r test.pcap -Y 'ftp' -T fields -e ip.src -e ip.dst -e tcp.srcport -e tcp.dstport -e ftp.request.command -e ftp.response.code -e tcp.stream -e ftp.request.arg -E header=n -E separator={separator_tshark}  "

    with open(output_file, "w") as outfile:
        subprocess.run(tshark_command, shell=True, stdout=outfile, text=True)

    with open(output_file_test, "w") as outfile:
         subprocess.run(tshark_command_test, shell=True, stdout=outfile, text=True)



# filter_ftp_commands(output_file, output_file_test)
two_d_array = read_file_into_2d_array(output_file)
clean_two_d_array_train = clean_array(two_d_array)
two_d_array_test = read_file_into_2d_array(output_file_test)
clean_two_d_array_test = clean_array(two_d_array_test,10000)

#clean_two_d_list = clean_two_d_array.tolist()
#clean_two_d_list_test = clean_two_d_array_test.tolist()
 
# clean_two_d_list.extend(clean_two_d_list_test)
#write_2d_array_to_excel(clean_two_d_list, output_excel)
#write_2d_array_to_excel(clean_two_d_list_test, output_excel)



#sys.exit("Stopping the script here.")


#Ideen: Nicht mehr nach excel speichern -> muss nicht non ascii oder was das ist rauswerfen
        # use tokenizer -> raum von allen Zeichen ist mein feature space -> Zeichen die nicht im training vorkommen werden im test benutzt und sind nicht vorhanden
        # from tokens to benutzbare features => count ngram
        # wie paul sagt mal in das zusatztextfeld schaun in wireshark
        # maybe PCA


import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import IsolationForest
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import OneClassSVM

def is_four_letters(value):
    return isinstance(value, str) and len(value) <= 4 and len(value) >= 3 and value.isalpha() and value.isupper()

# Load the data
#data = pd.read_excel(output_excel, header=None, nrows=172313)
data = pd.DataFrame(clean_two_d_array_train, columns=['src_ip', 'dest_ip', 'src_port', 'dest_port', 'command', 'response_code', 'stream', 'arg'])
print("read data")
data.info(verbose = False, memory_usage = 'deep')
data.columns = ['src_ip', 'dest_ip', 'src_port', 'dest_port', 'command', 'response_code', 'stream', 'arg']

#data_classify = pd.read_excel(output_excel, header=None, skiprows=172313)
data_classify = pd.DataFrame(clean_two_d_array_test, columns=['src_ip', 'dest_ip', 'src_port', 'dest_port', 'command', 'response_code', 'stream', 'arg'])
print("read data")
print("read data")
data_classify.info(verbose = False, memory_usage = 'deep')
data_classify.columns = ['src_ip', 'dest_ip', 'src_port', 'dest_port', 'command', 'response_code', 'stream', 'arg']

working_data_classify = data_classify.drop(['src_ip', 'dest_ip', 'src_port', 'dest_port'], axis=1)
working_data_classify['is_four_letters'] = working_data_classify['command'].apply(is_four_letters)
working_data_classify.info(verbose = False, memory_usage = 'deep')

working_data = data.drop(['src_ip', 'dest_ip', 'src_port', 'dest_port'], axis=1)
working_data_classify['is_four_letters'] = working_data_classify['command'].apply(is_four_letters)
#working_data['command_arg_reponse'] = working_data_classify['command'] 
working_data.info(verbose = False, memory_usage = 'deep')

# Replace empty strings in 'arg' with a placeholder
working_data['arg'] = working_data['arg'].fillna('missing').replace('', 'missing')
working_data_classify['arg'] = working_data_classify['arg'].fillna('missing').replace('', 'missing')
working_data['arg'] = working_data['arg'].astype(str).apply(lambda x: x.encode('utf-8'))
working_data_classify['arg'] = working_data_classify['arg'].astype(str).apply(lambda x: x.encode('utf-8'))
print("replaced NAs")
working_data.info(verbose = False, memory_usage = 'deep')

# Extract 1-gram features from command
combined_commands = pd.concat([working_data['command'], working_data_classify['command']], axis=0)
command_vectorizer = CountVectorizer(analyzer='word', ngram_range=(1, 1))
command_vectorizer.fit(combined_commands)
command_features = command_vectorizer.transform(working_data['command'])
command_features_classify = command_vectorizer.transform(working_data_classify['command'])
print("processed command")

combined_response_code = pd.concat([working_data['response_code'], working_data_classify['response_code']], axis=0)
response_code_vectorizer = CountVectorizer(analyzer='word', ngram_range=(1, 1))
response_code_vectorizer.fit(working_data['response_code'].astype(str))
response_code_features = response_code_vectorizer.transform(working_data['response_code'].astype(str))
response_code_features_classify = response_code_vectorizer.transform(working_data_classify['response_code'].astype(str))
print("processed response")

combined_args = pd.concat([working_data['arg'], working_data_classify['arg']], axis=0)
# # Write the byte strings to a file
# with open('data.txt', 'wb') as f:
#     for bs in combined_args:
#         f.write(bs + b'\n')
# # Train the SentencePiece model
# spm.SentencePieceTrainer.train(input='data.txt', model_prefix='bpe', vocab_size=2000, character_coverage=1.0, model_type='bpe')

# Load the trained SentencePiece model
sp = spm.SentencePieceProcessor(model_file='bpe.model')

encoded_data = combined_args.apply(lambda x: sp.encode_as_pieces(x.decode('utf-8')))
encoded_ids = combined_args.apply(lambda x: sp.encode_as_ids(x.decode('utf-8')))

encoded_arg_train = working_data['arg'].apply(lambda x: sp.encode_as_pieces(x.decode('utf-8')))
encoded_arg_test = working_data_classify['arg'].apply(lambda x: sp.encode_as_pieces(x.decode('utf-8')))

encoded_arg_train_to_vec = encoded_arg_train.apply(lambda x: ' '.join(x))
encoded_arg_test_to_vec = encoded_arg_test.apply(lambda x: ' '.join(x))

encoded_arg_combined = encoded_data.apply(lambda x: ' '.join(x))


# arg_vectorizer = CountVectorizer(analyzer='char', ngram_range=(1, 1), lowercase=False)
# arg_vectorizer.fit(combined_args)
# arg_features = arg_vectorizer.transform(working_data['arg'])
# arg_features_classify = arg_vectorizer.transform(working_data_classify['arg'])
arg_vectorizer = CountVectorizer(analyzer='word', ngram_range=(1, 1), lowercase=False)
arg_vectorizer.fit(encoded_arg_combined)
arg_features = arg_vectorizer.transform(encoded_arg_train_to_vec)
arg_features_classify = arg_vectorizer.transform(encoded_arg_test_to_vec)
print("processed arg")



def length_letters(value):
    return len(value)

# Combine the features with the original dataframe
def aggregate_data(command_features, arg_features, response_code_features, working_data):
    command_df = pd.DataFrame(command_features.toarray(), columns=command_vectorizer.get_feature_names_out())
    arg_df = pd.DataFrame(arg_features.toarray(), columns=arg_vectorizer.get_feature_names_out())
    response_code_df = pd.DataFrame(response_code_features.toarray(), columns=response_code_vectorizer.get_feature_names_out())
    print("command")
    
    command_df.info(verbose = False, memory_usage = 'deep')
    print("arg df")
    arg_df.info(verbose = False, memory_usage = 'deep')
    print("response code")
    response_code_df.info(verbose = False, memory_usage = 'deep')
    
    #all_four_letters = command_df.applymap(is_four_letters).all(axis=1).astype(int)
    #command_df['all_four_letters'] = all_four_letters

    print("combined features")
    working_data = pd.concat([working_data, command_df, arg_df, response_code_df], axis=1)
    #print(working_data.columns)
    print(working_data.head(5))
    print(command_df.columns)
    #print(arg_df.columns)
    #print(response_code_df.columns)

    # aggregated_data = working_data.groupby(['stream']).agg({
    #     'response_code': ['nunique', 'mean','max', 'min', 'size'],  # Aggregations
    #     **{col: 'sum' for col in command_df.columns},
    #     **{col: 'sum' for col in arg_df.columns},
    #     **{col: 'sum' for col in response_code_df.columns},
    #     #'all_four_letters': 'max'
    # }).reset_index()

    # Identify numeric columns for appropriate aggregations
    numeric_cols = working_data.select_dtypes(include=['number']).columns

    # Define your custom aggregation functions
    agg_funcs = {
        #'response_code': ['nunique', 'max', 'min', 'size'],  # Aggregations
    }

    # Add 'sum' aggregation for command_df, arg_df, and response_code_df columns
    agg_funcs.update({col: 'sum' for col in command_df.columns})
    #agg_funcs.update({col: 'sum' for col in arg_df.columns})
    #agg_funcs.update({col: 'sum' for col in response_code_df.columns})

    # Ensure to handle non-numeric columns separately if any
    non_numeric_cols = working_data.select_dtypes(exclude=['number']).columns.difference(['stream'])

    # If there are non-numeric columns, add them with an appropriate aggregation (e.g., 'first')
    agg_funcs.update({col: 'first' for col in non_numeric_cols})

    # Apply the groupby and aggregation
    aggregated_data = working_data.groupby(['stream']).agg(agg_funcs).reset_index()

    print("aggregated data")
    return aggregated_data

aggregated_data = aggregate_data(command_features, arg_features, response_code_features, working_data)
aggregated_data_classify = aggregate_data(command_features_classify, arg_features_classify, response_code_features_classify, working_data_classify)

# Normalize the data
scaler = StandardScaler()
estimation_data = aggregated_data.drop(['stream'], axis=1)
aggregated_data_scaled = scaler.fit_transform(estimation_data)
print("fit transformed")

estimation_data_classify = aggregated_data_classify.drop(['stream'], axis=1)
aggregated_data_scaled_classify = scaler.transform(estimation_data_classify)



#model = IsolationForest(contamination=0.01) 
model = OneClassSVM(kernel='rbf', gamma='auto', nu=0.01)
model.fit(aggregated_data_scaled)


#print(aggregated_data_scaled[:5])
print("model fit")

def predict(aggregated_data_scaled,aggregated_data,data, file):
    anomalies = model.predict(aggregated_data_scaled)
    print("predicted")

    num_anomalies = sum(anomalies == -1)
    num_normal = sum(anomalies == 1)

    print("anomalies: " + str(num_anomalies))
    print("normals: " + str(num_normal))
    print("percentage " + str(num_anomalies/num_normal))

    # -1 indicates anomaly, 1 indicates normal
    aggregated_data['anomaly'] = anomalies

    # Convert -1 (anomaly) to 1 (malicious) and 1 (normal) to 0 (benign)
    aggregated_data['anomaly'] = aggregated_data['anomaly'].apply(lambda x: 1 if x == -1 else 0)

    # # Create the connection string format and result
    results = []
    for index, row in aggregated_data.iterrows():
        # frequently I also like to make it more readable like this
        goal = row["stream"]  
    # Convert `goal` to a compatible format for comparison
        goal = float(goal.iloc[0])  # Convert to float if necessary

    # Filter the DataFrame based on the comparison
        filtered_data = data[data['stream'] == goal]

        # Access the desired values if the filtered DataFrame is not empty
        if not filtered_data.empty:
            src_ip = filtered_data['src_ip'].iat[0]
            dest_ip = filtered_data['dest_ip'].iat[0]
            src_port = filtered_data['src_port'].iat[0]
            dest_port = filtered_data['dest_port'].iat[0]

            connection_str = f"{src_ip}:{src_port}->{dest_ip}:{dest_port}"
            result_str = f"{connection_str},{int(row['anomaly'].iloc[0])},{int(goal)}"
            #result_str = f"{connection_str},{int(row['anomaly'].iloc[0])}"
            results.append(result_str)

    # Write results to CSV file
    output_file = file
    with open(output_file, 'w') as f:
        for result in results:
            f.write(result + '\n')

    print(f"Results written to {output_file}")


predict(aggregated_data_scaled, aggregated_data, data, 'ftp_anomalies.csv')    
predict(aggregated_data_scaled_classify, aggregated_data_classify, data_classify, 'ftp_anomalies_classify.csv')    