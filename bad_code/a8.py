from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import IsolationForest
import os

# Directory containing the TCP flow files
tcpflow_directory = 'combined_tcpflow_output_train'
tcpflow_directory_test = 'combined_tcpflow_output_test'

# Read TCP flow files
tcp_flows = []
for filename in sorted(os.listdir(tcpflow_directory)):
    filepath = os.path.join(tcpflow_directory, filename)
    if os.path.isfile(filepath):
        with open(filepath, 'rb') as f:
            data = f.read()
            hex_data = data.hex()  # Convert binary data to a hexadecimal string
            tcp_flows.append(hex_data)
            # data = f.read()
            # tcp_flows.append(data)

tcp_flows_test = []
for filename in sorted(os.listdir(tcpflow_directory_test)):
    filepath = os.path.join(tcpflow_directory_test, filename)
    if os.path.isfile(filepath):
        with open(filepath, 'rb') as f:
            data = f.read()
            hex_data = data.hex()  # Convert binary data to a hexadecimal string
            tcp_flows_test.append(hex_data)
            # data = f.read()
            # tcp_flows_test.append(data)

# Define n for n-grams
n = 3

# Build an n-gram model pipeline
model = make_pipeline(
    TfidfVectorizer(analyzer='char', ngram_range=(1, 5)),
    StandardScaler(with_mean=False),
    IsolationForest(contamination=0.005, random_state=42)
)

combined_flows = tcp_flows_test + tcp_flows
model.fit(combined_flows)

# Detect anomalies
anomalies = model.predict(tcp_flows)
test_anomalies = model.predict(tcp_flows_test)

# Print the anomalies
results = ""
count_ones = anomalies.tolist().count(-1)
print(count_ones)
for filename, anomaly in zip(sorted(os.listdir(tcpflow_directory)), anomalies):
    results += (f"{filename}, {0 if anomaly == 1 else 1}"+ '\n')
with open('anooooomalies.csv', 'w') as f:
    f.write(results)

results = ""
count_ones = test_anomalies.tolist().count(-1)
print(count_ones)
for filename, anomaly in zip(sorted(os.listdir(tcpflow_directory_test)), test_anomalies):
    results += (f"{filename}, {0 if anomaly == 1 else 1}"+ '\n')
with open('anooooomalies_test.csv', 'w') as f:
    f.write(results)    