import os
import pandas as pd
from sklearn.ensemble import IsolationForest
from sklearn.preprocessing import StandardScaler
import numpy as np

def read_tcp_flow_files(directory):
    tcp_flows = []
    for filename in os.listdir(directory):
        filepath = os.path.join(directory, filename)
        if os.path.isfile(filepath):
            with open(filepath, 'rb') as f:
                data = f.read()
                tcp_flows.append(data)
    return tcp_flows

def preprocess_data(data, max_length):
    # Convert the byte string to a list of integers
    data = [int(byte) for byte in data]
    # Pad or truncate the data to a fixed length
    if len(data) > max_length:
        data = data[:max_length]
    else:
        data.extend([0] * (max_length - len(data)))
    return data

# Directory containing the TCP flow files
tcpflow_directory = 'train'

# Read TCP flow files
tcp_flows = read_tcp_flow_files(tcpflow_directory)

max_length = 1000
# Preprocess the data
preprocessed_data = [preprocess_data(flow, max_length) for flow in tcp_flows]

# Convert to a 2D numpy array
binary_data = np.array(preprocessed_data)

# Standardize the data
scaler = StandardScaler()
binary_data = scaler.fit_transform(binary_data)

# Train an anomaly detection model
model = IsolationForest(contamination=0.1, random_state=42)
model.fit(binary_data)

# Detect anomalies
anomalies = model.predict(binary_data)

# Print the anomalies
print("Anomalies:")
results = ""
for filename, anomaly in zip(os.listdir(tcpflow_directory), anomalies):
    results += (f"{filename}: {0 if anomaly == 1 else 1}"+ '\n')
with open('anooooomalies.csv', 'w') as f:
    f.write(results)
   #print(f"{filename}: {'Normal' if anomaly == 1 else 'Anomaly'}")