
import subprocess
from openpyxl import Workbook
import csv
import re


#separator_tshark = "\x1F"
separator_tshark = "!"
separator_python = "!"
def remove_non_printable_chars(s):
    return re.sub(r'[^\x20-\x7E]+', '', s)

def read_file_into_2d_array(file_path):
    two_d_array = []
    with open(file_path, "r") as file:
        for line in file:
            line = remove_non_printable_chars(line)
            fields = line.split(separator_python, 3)
            # Splitting line using both tabs and spaces
            fields = [field if field != " " else "" for field in fields]
            if len(fields) != 4: raise Exception("Too many values: " + str(len(fields)) + " " + str(fields)) 
            two_d_array.append(fields)
    
    return two_d_array

# Example usage

def write_2d_array_to_excel(two_d_array, output_excel):
    # Create a new workbook
    wb = Workbook()
    # Select the active worksheet
    ws = wb.active

    # Process the 2D array
    cleaned_array = []
    for i, row in enumerate(two_d_array):
        # Check if the current row only has values in the second and third columns
        if len(row) > 2 and not any(row[0:1]) and any(row[1:3]) and not any(row[3:]):
            if i > 0:
                # Move the values to the row above
                cleaned_array[-1][1] = row[1]
                cleaned_array[-1][2] = row[2]
            # Skip adding this row to the cleaned array
            continue
        # Otherwise, add the row to the cleaned array
        cleaned_array.append(row)

    # Write the cleaned 2D array to the worksheet
    for row in cleaned_array:
        ws.append(row)

    # Save the workbook
    wb.save(output_excel)

# Example usage

def concatenate_path(wd, arg):
    if wd == "/":
        return wd + arg
    else:
        return wd + "/" + arg


def find_paths_with_prefix(paths_dict, prefix):
    matching_paths = [path for path in paths_dict.keys() if path.startswith(prefix)]
    return matching_paths


def replace_all_parent_folders(path_from, path_to):
    new_folders = path_to.split("/")
    #print(folders)
    old_folders = path_from.split("/")
    repl_len = len(new_folders)

    #print("oldfolders: " + str(old_folders))
    #print("newfolders: " + str(new_folders))
    for i in range(repl_len):
        old_folders[i] = new_folders[i]

    return "/".join(old_folders)


def calculate_files(darray):
    rnfrom = None

    # for i in range(len(two_d_array) - 1):  # Iterate up to the second last element
    #     subarray2 = two_d_array[i + 1]

    wd = ""
    i = 0
    file_type = None
    for command, response, stream , arg in darray:
        if i >= len(darray) - 1: break
        subarray2 = darray[i + 1]
        res = subarray2[1]
        
        if response == 220: #server rdy for user, next line has user
            continue
        if command == 'USER':
            wd = ""
        if command == 'TYPE':
            pass
        if command == 'RETR':
            pass
        if command == 'QUIT':
            wd = ""
        if command == 'PWD':
            pass
        if command == 'PASV':
            pass
        if command == 'PASS':
            pass
        if command == 'CWD':
            if int(res) < 400:
                folders = arg.split("/")
                wd_folders = wd.split("/")
                for folder in folders:
                    if folder == ".":
                        continue
                    if folder == "..":
                        if len(wd_folders) > 0:
                            del wd_folders[-1]
                    else:
                        wd_folders.append(folder)      
                wd = "/".join(wd_folders)         
            
        if command == 'CDUP': #go to parent directory, we have dir info
            wd_folders = wd.split("/")
            if len(wd_folders) > 0:
                        del wd_folders[-1]
            wd = "/".join(wd_folders)
        if command == 'LIST':
            pass
        if command == 'RNTO':
            if int(res) < 400 and rnfrom != concatenate_path(wd, arg) :
                log.append("called RNTO with " + rnfrom + " to " + concatenate_path(wd, arg))
                if rnfrom == None: raise Exception("have not called rnfr succesfully.")
                if rnfrom in data:
                    file_type = data[rnfrom]
                    if file_type == "d": #rename every path of every thing in that directory, including files
                        paths = find_paths_with_prefix(data, rnfrom + "/") #Does NOT find the folder itself, since it does not end with /
                        for path in paths:
                            path_type = data[path]
                            new_name = replace_all_parent_folders(path, concatenate_path(wd, arg) )
                            # if path_type == "f":
                            #     print("processing path: " + path)
                            #     print("file_type: " + file_type)
                            #     print("new name: " + new_name)
                            #     print("old name: " + path)
                            log.append("renaming in folder from " + path + " to " + new_name)
                            data[new_name] = path_type
                            del data[path]
                        log.append("renaming folder from " + rnfrom + "to " + concatenate_path(wd, arg))     
                        data[concatenate_path(wd, arg)] = file_type    
                        del data[rnfrom]    
                    else: # only rename the file, this is sufficient  
                        log.append("renaming file from " + rnfrom + "to " + concatenate_path(wd, arg))     
                        data[concatenate_path(wd, arg)] = file_type    
                        del data[rnfrom]
                else:
                    not_yet_deleted_rename[rnfrom] = "f"
                    log.append("Could not find " + rnfrom + " to rename to" + concatenate_path(wd, arg))

                
        if command == 'RNFR':
            if int(res) < 400:
                rnfrom = concatenate_path(wd, arg)
            else:
                raise Exception("yo this file does not exist for rename" + concatenate_path(wd, arg))

        if command == 'MKD':
            if int(res) < 600: #If this does not succeed, we don't care since the folder must already exist
                data[concatenate_path(wd, arg)] = "d"
        if command == 'DELE':
            if int(res) < 400:
                #print("succesful deletion " + res + " arg: " + arg + " wd: " + wd)
                if concatenate_path(wd, arg) in data:
                    del data[concatenate_path(wd, arg)]
                else:
                    print(concatenate_path(wd, arg) + "not found in data")
                    not_yet_delete_del[concatenate_path(wd, arg)] = "f"
        if command == 'STOR':
            if int(res) < 400:
                #print("created file: " + concatenate_path(wd, arg))
                log.append("created file: " + concatenate_path(wd, arg))
                created[concatenate_path(wd, arg)] = "f"
                data[concatenate_path(wd, arg)] = "f"
                assert data[concatenate_path(wd, arg)] == "f"    
        i+=1
        
            


def filter_ftp_commands(input_file, output_file, output_file_test):

    #uftp.keyinfo.tstamp empty
    tshark_command = f"/opt/homebrew/bin/tshark -r train.pcap -Y 'ftp' -T fields -e ftp.request.command -e ftp.response.code -e tcp.stream -e ftp.request.arg -E header=n -E separator={separator_tshark}  "
    tshark_command_test = f"/opt/homebrew/bin/tshark -r test.pcap -Y 'ftp' -T fields -e ftp.request.command -e ftp.response.code -e tcp.stream -e ftp.request.arg -E header=n -E separator={separator_tshark} "

    with open(output_file, "w") as outfile:
        subprocess.run(tshark_command, shell=True, stdout=outfile, text=True)

    with open(output_file_test, "w") as outfile:
        subprocess.run(tshark_command_test, shell=True, stdout=outfile, text=True)

# Example usage
input_file = "train.pcap"
output_file = "ftp_commands.txt"
output_file_test = "ftp_commands_test.txt"
filter_ftp_commands(input_file, output_file, output_file_test)

two_d_array = read_file_into_2d_array(output_file)
two_d_array_train = read_file_into_2d_array(output_file_test)
two_d_array.extend(two_d_array_train)

output_excel = "output_file.xlsx" 
output_excel_train = "output_file_train.xlsx" 
write_2d_array_to_excel(two_d_array, output_excel)
#exit
#write_2d_array_to_excel(two_d_array_train, output_excel_train)
#quit
log = []
data = {}
not_yet_deleted_rename = {}
not_yet_delete_del = {}
created = {}
calculate_files(two_d_array)



# print("Current data:")
# for key, value in data.items():
#     print(f"String: {key}, Letter: {value}")

print(len(not_yet_deleted_rename))
print("Problems:")
# for key, value in not_yet_deleted.items():
#     del data[key]

with open('std_output.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    for key, value in data.items():
        writer.writerow([key, value])

with open('std_errors_rename.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    for key, value in not_yet_deleted_rename.items():
        writer.writerow([key, value])

with open('std_errors_del.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    for key, value in not_yet_delete_del.items():
        writer.writerow([key, value])

with open('std_errors_created.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    for key, value in created.items():
        writer.writerow([key, value])

with open('log.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    for value in log:
        writer.writerow([value])        

with open('std_out_err.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    for key, value in data.items():
        if key in not_yet_deleted_rename:
            writer.writerow([key, value])


for key, value in not_yet_delete_del.items():
        if key in data:
            print("not deleted, still present in data " + key)

# Now the deleted files should not be present in the output, or respectively the thingies 
                        




# check if we need to add subfolders

def extract_parent_folders(paths_dict):
    parent_folders = set()
    for path, value in paths_dict.items():
        # Split the path into its components
        if value == "f": continue
        components = path.split("/")
        # Collect all parent folders
        for i in range(1, len(components)):
            parent_folder = "/".join(components[:i])
            if parent_folder:
                parent_folders.add(parent_folder)
    return parent_folders

def check_missing_parent_folders(paths_dict):
    parent_folders = extract_parent_folders(paths_dict)
    # List of missing parent folders
    missing_folders = [folder for folder in parent_folders if folder not in paths_dict]
    return missing_folders

missing_folders = check_missing_parent_folders(data)
print("printing how many folders are missing now")
print(len(missing_folders))

for folder in missing_folders:
    data[folder] = "d"


with open('std_output_final.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    for key, value in data.items():
        writer.writerow([key, value])


# import csv

# # Function to read CSV file into a dictionary
# def read_csv_file(filename):
#     data = {}
#     with open(filename, 'r') as file:
#         reader = csv.reader(file)
#         for row in reader:
#             data[row[0]] = row[1]
#     return data

# # Function to write missing entries to a new CSV file
# def write_missing_entries(filename, missing_entries):
#     with open(filename, 'w', newline='') as file:
#         writer = csv.writer(file)
#         for key, value in missing_entries.items():
#             writer.writerow([key, value])

# # Read the contents of std_output_final.csv and dings.csv into dictionaries
# std_output_final = read_csv_file('std_output_final.csv')
# dings = read_csv_file('result_task_1_complete_20240524_214254.csv')

# # Find missing entries in std_output_final.csv
# missing_in_std_output_final = {key: value for key, value in dings.items() if key not in std_output_final}

# # Find missing entries in dings.csv
# missing_in_dings = {key: value for key, value in std_output_final.items() if key not in dings}

# # Write missing entries to new CSV files
# write_missing_entries('missing_in_std_output_final.csv', missing_in_std_output_final)
# write_missing_entries('missing_in_dings.csv', missing_in_dings)        