from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import IsolationForest
import os
from scipy.sparse import hstack
import re
def extract_features(data):
    commands = []
    arguments = []
    responses = []
    messages = []
    
    for i, line in enumerate(data):
        if line == "\n": continue
        if i % 2 == 1:
            parts = line.split(maxsplit=1)
            command = parts[0]
            argument = parts[1] if len(parts) > 1 else ""
            commands.append(command)
            arguments.append(argument)
        else:
            parts = line.split(maxsplit=1)
            response = parts[0]
            message = parts[1] if len(parts) > 1 else ""
            responses.append(response)
            messages.append(message)

    return commands, arguments, responses, messages

# Directory containing the TCP flow files
tcpflow_directory = 'combined_tcpflow_output_train'
tcpflow_directory_test = 'combined_tcpflow_output_test'

def read_tcp_flows(tcpflow_directory):
    tcp_flows_hex = []
    tcp_flows_ascii = []
    for filename in sorted(os.listdir(tcpflow_directory)):
        filepath = os.path.join(tcpflow_directory, filename)
        if os.path.isfile(filepath):
            with open(filepath, 'rb') as f:
                    data = f.read()
                    hex_data = data.hex()  # Convert binary data to a hexadecimal string
                    tcp_flows_hex.append(hex_data)
            with open(filepath, 'r') as f:
                try:
                    data_ascii = data.decode('ascii')
                    lines = f.readlines()
                    commands, arguments, responses, messages = extract_features(lines)
                    tcp_flows_ascii.append(data_ascii)    
                except UnicodeDecodeError:
                    tcp_flows_ascii.append("x")
                    print(f"UnicodeDecodeError: {filename}")

    return tcp_flows_hex,tcp_flows_ascii

tcp_flows_test = []
# for filename in sorted(os.listdir(tcpflow_directory_test)):
#     filepath = os.path.join(tcpflow_directory_test, filename)
#     if os.path.isfile(filepath):
#         with open(filepath, 'rb') as f:
#             data = f.read()
#             hex_data = data.hex()  # Convert binary data to a hexadecimal string
#             tcp_flows_test.append(hex_data)
#             # data = f.read()
#             # tcp_flows_test.append(data)

# Define n for n-grams
n = 3



# Build an n-gram model pipeline
model = make_pipeline(
    TfidfVectorizer(analyzer='char', ngram_range=(1, 1)),
    #StandardScaler(with_mean=False),
    IsolationForest(contamination=0.005, random_state=42)
)

tcp_flows_test, tcp_flows_test_ascii = read_tcp_flows(tcpflow_directory_test)
tcp_flows, tcp_flows_ascii = read_tcp_flows(tcpflow_directory)
combined_flows = tcp_flows_test + tcp_flows
combined_flow_ascii = tcp_flows_test_ascii + tcp_flows_ascii

hex_vectorizer = TfidfVectorizer(analyzer='char', ngram_range=(1, 1))
ascii_vectorizer = TfidfVectorizer(analyzer='char', ngram_range=(1, 1))
hex_vectorizer.fit(combined_flows)
ascii_vectorizer.fit(combined_flow_ascii)

features_1 = hex_vectorizer.fit_transform(combined_flows)
features_2 = ascii_vectorizer.fit_transform(combined_flow_ascii)
combined_features = hstack([features_1, features_2])

model = IsolationForest(contamination=0.005, random_state=42)
model.fit(combined_features)
#model.fit(combined_flows)


features_1 = hex_vectorizer.transform(tcp_flows)
features_2 = ascii_vectorizer.transform(tcp_flows_ascii)
combined_features = hstack([features_1, features_2])
anomalies = model.predict(combined_features)

# Detect anomalies
features_1 = hex_vectorizer.transform(tcp_flows_test)
features_2 = ascii_vectorizer.transform(tcp_flows_test_ascii)
combined_features = hstack([features_1, features_2])
test_anomalies = model.predict(combined_features)

# Print the anomalies
results = ""
count_ones = anomalies.tolist().count(-1)
print(count_ones)
print(f"{count_ones/len(anomalies)}")
for filename, anomaly in zip(sorted(os.listdir(tcpflow_directory)), anomalies):
    results += (f"{filename}, {0 if anomaly == 1 else 1}"+ '\n')
with open('anooooomalies.csv', 'w') as f:
    f.write(results)

def remove_leading_zeros(string):
    pattern = r'(?<=\.)0+(?=\d)'
    return re.sub(pattern, '', string)

results = ""
count_ones = test_anomalies.tolist().count(-1)
print(count_ones)
print(f"{count_ones/len(test_anomalies)}")
for filename, anomaly in zip(sorted(os.listdir(tcpflow_directory_test)), test_anomalies):
    modified_filename = remove_leading_zeros(filename)
    # Split the string by the dash (-) to separate IP addresses and port numbers
    ip1, ip2 = modified_filename.split('-')
    # Find the index of the last dot in the first IP address
    last_dot_index_ip1 = ip1.rfind('.')
    # Replace the dot at the index before the last dot in the first IP address with a colon
    ip1_with_colon = ip1[:last_dot_index_ip1] + ':' + ip1[last_dot_index_ip1 + 1:]
    # Find the index of the last dot in the second IP address
    last_dot_index_ip2 = ip2.rfind('.')
    # Replace the dot at the index before the last dot in the second IP address with a colon
    ip2_with_colon = ip2[:last_dot_index_ip2] + ':' + ip2[last_dot_index_ip2 + 1:]
    output_string = f"{ip1_with_colon}->{ip2_with_colon}"

    results += (f"{output_string},{0 if anomaly == 1 else 1}"+ '\n')
with open('anooooomalies_test.csv', 'w') as f:
    f.write(results)    


